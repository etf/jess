from __future__ import print_function
from future.utils import with_metaclass

import abc
import os.path
import pickle
import logging
import time
import base64

import nap.core

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse
import shutil

import jess.api

jess_log = logging.getLogger('jess')


class Job(object):
    def __init__(self):
        self.job_id = None
        self.status = None          # status id (backend dependent)
        self.verbose_status = None  # status as string
        self.status_out = None      # status details
        self.state = None           # state (jess internal)
        self.submit_cmd = None      # job submit cmd
        self.details = None         # error details
        self.submit_out = None      # job submit output
        self.log = None             # job log
        self.output = None          # job stdout
        self.resource = None        # resource uri
        # meta
        self.wd = None
        self.backend = None
        self.proxy = None
        self.proxy_details = None
        self.token = None
        self.token_details = None
        self.jdl = None
        self.payload = None
        self.job_file = None
        self.arc_job_file = None
        self.arc_resource = None
        self.pool = None
        self.schedd = None
        self.status_failed = 0  # unused ?
        # ts
        self.ts_job_start = None
        self.ts_job_end = None

    @classmethod
    def load(cls, job_file):
        with open(job_file, 'rb') as jf:
            job_obj = pickle.load(jf)
            job_obj.job_file = job_file
        return job_obj

    @classmethod
    def save(cls, obj):
        assert obj.job_file
        with open(obj.job_file, 'wb') as jf:
            pickle.dump(obj, jf)

    def __str__(self):
        return 'Job: ({}) {} {}\n'.format(self.job_id, self.status, self.state)


class JMI(object):
    # Job Management Interface (common backend interface)
    # todo: refactor _resource business into a common uri across all backends

    def __init__(self, working_dir, backend, jdl, payload, proxy=None, token=None, resource=None,
                 pool=None, schedd=None, arc_wd=None, arc_debug='INFO',
                 arc_sub_type=None, arc_info_type=None, arc_registry=None, arc_ce=None,
                 excludes=['.pyc', '.pyo', '.git']):
        jess_log.debug('JMI init:')
        self._jdl = jdl
        self._working_dir = working_dir
        self._schedd = schedd
        self._resource = resource
        self._excludes = excludes
        self._pool = pool
        self._proxy = proxy
        if isinstance(token, bytes):
            token = token.decode()
        self._token = token
        self._backend = backend
        self._payload = payload
        # ARC specials
        if arc_wd:
            self._arc_wd = arc_wd
        else:
            self._arc_wd = working_dir
        self._arc_resource = None
        self._arc_debug = arc_debug
        self._arc_comp_element = arc_ce
        self._arc_sub_type = arc_sub_type
        self._arc_info_type = arc_info_type
        self._arc_registry = arc_registry
        self._arc_job_file = None
        if self._backend == 'arc':
            self._arc_job_file = os.path.join(self._arc_wd, 'jobs.dat')
        if self._backend == 'arc' and self._resource:
            self._arc_resource = urlparse(self._resource).netloc.split(':')[0]

        jess_log.debug('    Backend: {}'.format(self._backend))
        jess_log.debug('    Resource: {}'.format(self._resource))
        jess_log.debug('    JDL: {}'.format(self._jdl.jdl_path))
        if self._proxy:
            jess_log.debug('    Proxy: {}'.format(self._proxy))
        if self._token:
            jess_log.debug('    Token: {}'.format(self._token))
        jess_log.debug('    Payload: {}'.format(self._payload.tar_file))

        # sanity
        if not os.path.exists(working_dir):
            os.makedirs(working_dir)

        if self._proxy and not os.path.exists(self._proxy):
            raise jess.JessConfigurationError("Failed to find proxy {}".format(self._proxy))
        if self._token and not os.path.exists(self._token):
            raise jess.JessConfigurationError("Failed to find token {}".format(self._token))

        if (self._backend == 'cream' or self._backend == 'arc') and not self._resource:
            raise jess.JessConfigurationError("CREAM/ARC backend requires an explicit resource argument")

        try:
            self._backend_obj = getattr(jess.api, self._backend)
        except AttributeError:
            raise jess.JessConfigurationError("Unable to load backend {}".format(self._backend))

        # env
        if self._proxy:
            os.environ['X509_USER_PROXY'] = self._proxy

    @staticmethod
    def proxy_info(proxy):
        if not proxy:
            return ''
        try:
            ret_code, cmd_out = nap.core.sub_process('/bin/voms-proxy-info --subject --fqan --file {}'.format(proxy),
                                                     mode='pexpect')
            if ret_code != nap.core.SUBPROCESS_FAILED:
                return cmd_out
        except Exception as e:
            return 'Failed to run voms-proxy-info {}'.format(e)

    @staticmethod
    def token_info(token_file):
        if not token_file:
            return ''
        try:
            with open(token_file, 'r') as tf:
                token = tf.read()
            if '.' in token:
                token = token.split(".")[1]
                padding = 4 - (len(token) % 4)
                return base64.b64decode(token + padding * "=")
        except Exception as e:
            return 'Failed to get token info {}'.format(e)

    def submit(self):
        # submits new job and adds it to the jobs file
        jess_log.debug('         submitting new job')
        kwargs = dict()
        args = list()
        if self._backend == 'arc':
            kwargs['joblist'] = self._arc_job_file
            if self._arc_registry or self._arc_info_type or self._arc_sub_type or self._arc_comp_element:
                if self._arc_comp_element:
                    kwargs['computing-element'] = self._arc_comp_element
                else:
                    kwargs['computing-element'] = self._arc_resource
                if self._arc_registry:
                    kwargs['registry'] = self._arc_registry
                if self._arc_sub_type:
                    kwargs['submission-endpoint-type'] = self._arc_sub_type
                if self._arc_info_type:
                    kwargs['info-endpoint-type'] = self._arc_info_type
            else:
                kwargs['computing-element'] = self._arc_resource
            kwargs['timeout'] = 120
            kwargs['debug'] = self._arc_debug
            # kwargs['info-endpoint-type'] = 'NONE'
        if self._backend == 'cream':
            args.append('autm-delegation')
            args.append('debug')
            kwargs['resource'] = self._resource
        if self._backend == 'scondor' and self._pool:
            kwargs['name'] = self._schedd
            kwargs['pool'] = self._pool
        if self._backend == 'scondor' and self._resource and 'condor-ce:' in self._resource:
            args.append('spool')

        # payload tarball
        jess_log.debug('         creating payload at {}'.format(self._payload.tar_file))
        self._payload.tar(exclude=lambda ti: None if os.path.splitext(ti.name)[1] in self._excludes else ti)

        j = self._backend_obj.submit(self._jdl, *args, **kwargs)
        jess_log.debug('         job successfully submitted {}'.format(j.job_id))
        job = Job()
        # job
        job.job_id = j.job_id
        job.state = 'submit'
        job.submit_out = j.details
        job.submit_cmd = j.cmd
        # meta
        job.backend = self._backend
        job.proxy = self._proxy
        # only if job.hasattr(proxy_details) !
        if hasattr(job, 'proxy_details'):
            job.proxy_details = JMI.proxy_info(self._proxy)
        job.token = self._token
        if hasattr(job, 'token_details'):
            job.token_details = JMI.token_info(self._token)
        job.jdl = self._jdl
        job.payload = self._payload
        job.resource = self._resource
        if self._backend == 'arc':
            job.arc_job_file = self._arc_job_file
            job.arc_resource = self._arc_resource
        job.pool = self._pool
        job.schedd = self._schedd
        job.ts_job_start = time.time()
        return job

    def status(self, job_id):
        kwargs = dict()
        if self._backend == 'arc':
            kwargs['joblist'] = self._arc_job_file
            kwargs['timeout'] = 120
        if self._backend == 'scondor' and self._pool:
            kwargs['name'] = self._schedd
            kwargs['pool'] = self._pool

        return self._backend_obj.status(job_id, **kwargs)

    def log(self, job_id):
        kwargs = dict()
        if self._backend == 'scondor' and self._pool:
            kwargs['name'] = self._schedd
            kwargs['pool'] = self._pool
        if self._backend == 'scondor' and 'log' in self._jdl.keys():
            return self._backend_obj.logs(job_id, log=self._jdl['log'], **kwargs)
        elif self._backend == 'arc':
            return self._backend_obj.logs(job_id, joblist=self._arc_job_file)
        else:
            return self._backend_obj.logs(job_id, **kwargs)

    def output(self, job_id):
        kwargs = dict()
        kwargs['dir'] = os.path.join(self._working_dir, 'out')
        if self._backend == 'arc':
            kwargs['joblist'] = self._arc_job_file
        tmp_dir = self._backend_obj.output(job_id, **kwargs)
        if self._backend == 'cream' or self._backend == 'arc':
            # copy output
            out_dir = os.path.join(self._working_dir, 'out')
            for f in os.listdir(tmp_dir):
                ff = os.path.join(tmp_dir, f)
                if os.path.isfile(ff):
                    shutil.copy(ff, out_dir)
        return tmp_dir

    def transfer_data(self, job_id):
        kwargs = dict()
        if self._backend == 'scondor' and self._pool:
            kwargs['name'] = self._schedd
            kwargs['pool'] = self._pool

        self._backend_obj.transfer_data(job_id, **kwargs)

    def cancel(self, job_id):
        kwargs = dict()
        if self._backend == 'arc':
            kwargs['joblist'] = self._arc_job_file
        if self._backend == 'scondor' and self._pool:
            kwargs['name'] = self._schedd
            kwargs['pool'] = self._pool

        return self._backend_obj.cancel(job_id, **kwargs)

    def purge(self, job_id):
        kwargs = dict()
        if self._backend == 'arc':
            kwargs['joblist'] = self._arc_job_file
        if self._resource and 'condor-ce:' in self._resource:  # don't purge remote queues
            return
        if self._backend == 'scondor':  # condor needs cancel first
            self.cancel(job_id)
        if self._backend == 'scondor' and self._pool:
            kwargs['name'] = self._schedd
            kwargs['pool'] = self._pool

        return self._backend_obj.purge(job_id, **kwargs)

    def completed(self, status):
        return self._backend_obj.completed(status)

    def failed(self, status):
        return self._backend_obj.failed(status)

    def is_terminal(self, status):
        return self._backend_obj.is_terminal(status)


class Tracker(object):
    # submits/tracks jobs for particular backend
    # todo: remove arc specific args

    def __init__(self, wd, jmi, job_schedule=60):
        jess_log.debug('Tracker init:')

        self._observers = set()
        self._job_schedule = int(job_schedule)
        self._jmi = jmi
        self._working_dir = wd
        self._job_file = os.path.join(self._working_dir, 'jobs.pkl')

        jess_log.debug('    Working directory: {}'.format(self._working_dir))

    def run(self):
        #
        # main loop
        # checks if job file exists or runs submit
        # if there is a job file, checks state vs. schedule and/or polls for status
        #    if job is successful/completed - gets output, gets logs, runs purge
        #    if job failed - gets output, gets logs, runs purge
        #    if job is in terminal state (but unclear if failed or completed) - attempts purge
        # job file is removed only if job is in terminal state and next job can run accord. to defined schedule
        #
        # job.state - submitted -> idle -> completed/failed/unknown -> waiting (for new submit)
        # job.status - stores the job state as returned by the backend (it's backend dependent)
        #
        jess_log.debug('Tracker: run')
        if os.path.exists(self._job_file):
            jess_log.debug('         loading job file {}'.format(self._job_file))
            try:
                job = Job.load(self._job_file)
            except (EOFError, ValueError) as e:
                # job file probably corrupted; remove it and re-submit new job
                os.remove(self._job_file)
                job = self._jmi.submit()
                job.job_file = self._job_file
                job.wd = self._working_dir
                jess_log.debug('         writing job file at {}'.format(self._job_file))
                Job.save(job)
                return job

            jess_log.debug('         check schedule {}'.format(self._job_schedule * 60))
            if job.ts_job_end:
                if int(time.time() - job.ts_job_start) >= self._job_schedule * 60:
                    # previous job finished and job schedule says it's time to start a new one
                    jess_log.debug('        -> ts_job_end: {} delay: {}'.format(job.ts_job_end,
                                                                                int(time.time() - job.ts_job_start)))
                    os.remove(self._job_file)
                    job = self._jmi.submit()
                    job.job_file = self._job_file
                    job.wd = self._working_dir
                    jess_log.debug('         writing job file at {}'.format(self._job_file))
                    Job.save(job)
                    return job
                else:
                    # waiting for next time slot to submit new job
                    jess_log.debug('        -> ts_job_end: {} delay: {}'.format(job.ts_job_end,
                                                                                int(time.time() - job.ts_job_start)))
                    jess_log.debug('           waiting for next time slot')
                    job.state = 'waiting'
                    Job.save(job)
                    return job

            jess_log.debug('         polling status for job id: {}'.format(job.job_id))
            try:
                st = self._jmi.status(job.job_id)
            except jess.JobStatusError as e:
                jess_log.debug('Failed to get job status ({}/{})'.format(e.ret_code, e.details))
                if int(e.ret_code) == 0 or (job.backend == 'arc' and int(e.ret_code) == 1):
                    jess_log.debug('   Status failed with return code 0, job probably lost, re-submitting')
                    job.ts_job_end = time.time()
                    job.state = "unknown"
                    job.status = 3
                    job.verbose_status = 'Failed to get job status, but no error reported, will re-submit a new one'
                    job.status_out = e.details
                    Job.save(job)
                raise
            job.status = st.status
            job.verbose_status = st.ext_status
            job.status_out = st.details
            self._notify(job)   # notify observers on status changes

            if self._jmi.completed(st.status):
                # finished fine
                jess_log.debug('         job completed with status {} ({})'.format(st.status, job.job_id))
                # os.remove(self._job_file)
                job.state = "completed"
                job.ts_job_end = time.time()
                try:
                    out_dir = self._jmi.output(job.job_id)
                    job.output = out_dir
                    jess_log.debug('         job output stored in {}'.format(out_dir))
                except jess.JobOutputError as e:
                    jess_log.debug('   Job completed but failed to get output')
                    job.state = "failed"
                    job.verbose_status = 'Job completed but failed to get job output'
                    job.output = None
                    job.status_out = e.details
                log = self._jmi.log(job.job_id)
                job.log = log.log
                Job.save(job)
                jess_log.debug('         successfully retrieved job log')
                if job.resource and 'condor-ce:' in job.resource:
                    jess_log.debug('        retrieving data')
                    self._jmi.transfer_data(job.job_id)
                jess_log.debug('         purging the job')
                # self._cancel(job.job_id)
                self._jmi.purge(job.job_id)
                return job
            elif self._jmi.failed(st.status):
                # failed
                jess_log.debug('         job failed with status {} ({})'.format(st, job.job_id))
                # os.remove(self._job_file)
                job.state = "failed"
                job.ts_job_end = time.time()
                log = self._jmi.log(job.job_id)
                jess_log.debug('         successfully retrieved job log')
                job.log = log.log
                if job.backend == 'arc':
                    try:   # attempt to get output for arc even if job failed (to get gmlog)
                        out_dir = self._jmi.output(job.job_id)
                        job.output = out_dir
                        jess_log.debug('         successfully retrieved job output')
                    except jess.JobOutputError as e:
                        jess_log.debug('         failed to retrieve job output')
                Job.save(job)
                jess_log.debug('         purging the job')
                # self._cancel(job.job_id)
                self._jmi.purge(job.job_id)
                return job
            elif self._jmi.is_terminal(st.status):
                # neither finished nor failed but terminal ?
                jess_log.debug('         job terminated with status {} ({})'.format(st, job.job_id))
                # os.remove(self._job_file)
                job.state = "unknown"
                job.ts_job_end = time.time()
                log = self._jmi.log(job.job_id)
                jess_log.debug('         successfully retrieved job log')
                job.log = log.log
                Job.save(job)
                return job
            else:
                jess_log.debug('         job in status {} ({})'.format(st, job.job_id))
                job.state = "idle"
                if job.backend == 'scondor':
                    log = self._jmi.log(job.job_id)
                    job.log = log.log
                Job.save(job)
                return job
        else:
            job = self._jmi.submit()
            job.job_file = self._job_file
            job.wd = self._working_dir
            jess_log.debug('         writing job file at {}'.format(self._job_file))
            Job.save(job)
            return job

    def attach(self, observer):
        observer._subject = self
        self._observers.add(observer)

    def detach(self, observer):
        observer._subject = None
        self._observers.discard(observer)

    def _notify(self, job):
        for observer in self._observers:
            observer.update(job)


class EventsTracker(Tracker):
    # tracks jobs based on status events instead of polling
    pass


class Observer(with_metaclass(abc.ABCMeta)):
    """
    Define an updating interface for objects that should be notified of
    changes in a subject.
    """

    def __init__(self):
        self._subject = None

    @abc.abstractmethod
    def update(self, job):
        pass


class Timer(Observer):
    # tracks time spent in different job states
    # calculates ranges and checks them wrt. those passed by args
    def __init__(self, limits={}):
        super(Timer, self).__init__()
        self._limits = limits

    def check_limits(self, job):
        status = job.verbose_status
        jess_log.debug('Checking limits:')
        if job.ts_job_end:   # don't check finished job
           return False
        if 'global' in self._limits.keys():
            jess_log.debug('    global: {} {}'.format(time.time() - job.ts_job_start, self._limits['global'] * 60))
        if 'global' in self._limits.keys() and time.time() - job.ts_job_start >= self._limits['global'] * 60:
            job.status_out = 'Global job timeout ({} minutes) was reached, canceling the job'.format(
                                                                                 self._limits['global'])
            return True
        if not self._limits or not hasattr(job, 'timer') or status not in self._limits.keys():
            return False
        jess_log.debug('    status: {} {}'.format(time.time() - job.timer[status], self._limits[status] * 60))
        if (time.time() - job.timer[status]) >= self._limits[status] * 60:
            job.status_out = 'Job timed out, {} minutes spent in status {}'.format(
                                (time.time() - job.timer[status]) / 60, status)
            return True

    def update(self, job):
        if not self._limits:
            return
        if not hasattr(job, 'timer'):  # add custom attributes
            job.timer = dict()

        if job.verbose_status and job.verbose_status not in job.timer.keys():
            job.timer[job.verbose_status] = time.time()
        jess_log.debug('Job timer: {}'.format(job.timer))
