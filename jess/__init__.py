from __future__ import print_function

AUTHOR = "Marian Babik"
AUTHOR_EMAIL = "<marian.babik@cern.ch>"
COPYRIGHT = "Copyright (C) 2015 CERN"
VERSION = "0.2.58"
DATE = "31 July 2018"
__author__ = AUTHOR
__version__ = VERSION
__date__ = DATE


class JessError(Exception):
    def __init__(self, msg, details=None, ret_code=None, cmd=None):
        self.msg = msg
        self.details = details
        self.ret_code = ret_code
        self.cmd = cmd

    def __repr__(self):
        print(self.msg, self.cmd, self.ret_code, self.details)


class JobSubmitError(JessError): pass


class JobCancelError(JessError): pass


class JobPurgeError(JessError): pass


class JobStatusError(JessError): pass


class JobLogError(JessError): pass


class JobOutputError(JessError): pass


class JessException(Exception): pass


class JessPayloadException(JessException): pass


class JessConfigurationError(JessError): pass
