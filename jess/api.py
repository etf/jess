from past.builtins import basestring

import tarfile
import os
import collections

import jess
import jess.backends.condor
import jess.backends.scondor
import jess.backends.cream
import jess.backends.arc


htcondor = jess.backends.condor.HTCondor()

scondor = jess.backends.scondor.HTCondor()

cream = jess.backends.cream.CREAM()

arc = jess.backends.arc.ARC()


class Payload(object):
    def __init__(self, tar_file):
        self.tar_file = tar_file
        if not os.path.exists(os.path.dirname(self.tar_file)):
            raise jess.JessPayloadException("Target directory for tarball {} doesn't exist".format(self.tar_file))
        self.dirs_to_pack = list()

    def add_dirs(self, *dirs, **kwargs):
        prefix = kwargs.pop('prefix', None)
        assert not isinstance(dirs, basestring)
        for d in dirs:
            if not os.path.exists(d):
                raise jess.JessPayloadException("Path {} doesn't exist while adding to tarball".format(d))
        for d in dirs:
            self.dirs_to_pack.append((d, prefix))

    def add(self, path, prefix=None):
        if not os.path.exists(path):
            raise jess.JessPayloadException("Path {} doesn't exist while adding to tarball".format(path))

        self.dirs_to_pack.append((path, prefix))

    def tar(self, exclude=None):
        with tarfile.open(self.tar_file, "w|gz") as tar:
            for path, prefix in self.dirs_to_pack:
                tar.add(path, arcname=prefix, filter=exclude)

    def zero_file(self):
        open(self.tar_file, 'w').close()


class JDL(collections.OrderedDict):

    def __init__(self, *args, **kwargs):
        super(JDL, self).__init__(*args, **kwargs)
        self.jdl_path = None

    def serialize(self, out_file, backend, jdl_ads=None):
        self.jdl_path = out_file
        with open(out_file, 'w') as out:
            if backend == 'cream':
                out.write('[\n')
                for k, v in self.items():
                    if isinstance(v, basestring):
                        out.write('{} = "{}";\n'. format(k, v))
                    else:
                        fl = '{'
                        fl += ', '.join(['"{}"'.format(i) for i in v])
                        fl += '}'
                        out.write('{} = {};\n'.format(k, fl))
                out.write(']\n')
                return
            elif backend == 'arc':
                out.write('&')
                for k, v in self.items():
                    if isinstance(v, basestring):
                        if k == 'arguments':
                            out.write('({}={})\n'.format(k, " ".join(['"{}"'.format(s) for s in v.split(" ")])))
                        else:
                            out.write('({}="{}")\n'.format(k, v))
                    else:
                        fl = ' '.join(['("{}" "")'.format(i) for i in v])
                        out.write('({}= {})\n'.format(k, fl))
                if jdl_ads:
                    out.write('\n'.join(jdl_ads))
                    out.write('\n')
                return
            elif backend in ['condor', 'scondor']:
                for k, v in self.items():
                    if k == 'arguments':
                        out.write('{} = "{}"\n'.format(k, v))
                        continue
                    if isinstance(v, basestring):
                        out.write('{} = {}\n'. format(k, v))
                    else:
                        out.write('{} = {}\n'.format(k, ', '.join(v)))
                if jdl_ads:
                    out.write('\n'.join(jdl_ads))
                    out.write('\n')
                out.write('queue\n')
                return
            else:
                raise jess.JessException("Serialization to unsupported backend {}".format(backend))
