import logging
import os
import pexpect

log = logging.getLogger('jess')


def run(cmd, cmd_timeout=300, log_file=None):
    log.debug("    Subprocess %s starting" % cmd)
    try:
        cmd_out, ret_code = pexpect.run(' '.join(cmd), timeout=cmd_timeout, withexitstatus=True, env=os.environ,
                                        logfile=log_file)
    except pexpect.ExceptionPexpect as e:
        log.debug("    Subprocess %s failed with (%s)" % (cmd, e))
        return False, ret_code, cmd_out

    log.debug("    Subprocess {} ({}) finished".format(cmd, ret_code))
    log.debug("    Subprocess output: %s " % cmd_out)

    return True, ret_code, cmd_out
