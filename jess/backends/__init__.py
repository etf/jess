from __future__ import print_function
from future.utils import with_metaclass
from collections import namedtuple
import abc


class PluginBase(with_metaclass(abc.ABCMeta)):
    def __init__(self):
        self.Job = namedtuple('Job', 'job_id details log_file cmd')
        self.Job.__new__.__defaults__ = (None, None, None, None)

        self.Log = namedtuple('Log', 'job_id log')
        self.Log.__new__.__defaults__ = (None, None)

        self.Status = namedtuple('Status', 'job_id status ext_status details')
        self.Status.__new__.__defaults__ = (None, None, None, None)

    @staticmethod
    def _process_args(cmd, *args, **kwargs):
        for item in args:
            cmd.append('--%s' % item)
        for key, value in kwargs.items():
            cmd.append('--%s %s' % (key, value))
        return cmd

    @staticmethod
    def _process_legacy_args(cmd, *args, **kwargs):
        for item in args:
            cmd.append('-%s' % item)
        for key, value in kwargs.items():
            cmd.append('-%s %s' % (key, value))
        return cmd

    @abc.abstractmethod
    def submit(self, jdl, **kwargs):
        pass

    @abc.abstractmethod
    def cancel(self, job, **kwargs):
        pass

    @abc.abstractmethod
    def purge(self, job, **kwargs):
        pass

    @abc.abstractmethod
    def status(self, job, **kwargs):
        pass

    @abc.abstractmethod
    def logs(self, job, **kwargs):
        pass

    @abc.abstractmethod
    def output(self, job, **kwargs):
        pass
