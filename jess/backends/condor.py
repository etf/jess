from jess.backends import PluginBase


class HTCondor(PluginBase):
    def __init__(self, *args, **kwargs):
        super(HTCondor, self).__init__(*args, **kwargs)

    def submit(self, jdl):
        pass

    def cancel(self):
        pass

    def purge(self):
        pass

    def status(self):
        pass

    def logs(self):
        pass

    def output(self):
        pass
