from __future__ import print_function
import logging
import re

import jess
import jess.backends
import jess.subproc
import jess.settings as config

log = logging.getLogger('jess')


class ARC(jess.backends.PluginBase):
    def submit(self, jdl, *args, **kwargs):
        cmd = self._process_args(['arcsub'], *args, **kwargs)
        cmd.append('%s' % jdl.jdl_path)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)
        stdout = stdout.decode()
        if not stat:
            raise jess.JobSubmitError("Failed to submit the job", ret_code=ret_code, details=stdout, cmd=' '.join(cmd))
        try:
            job_id = re.search('Job submitted with jobid: (\S+)', stdout, re.M).groups()[0].rstrip()
        except (IndexError, AttributeError):
            # command succeeded but there is no job ID
            raise jess.JobSubmitError("Job submission has failed",
                                      ret_code=ret_code, details=stdout, cmd=' '.join(cmd))

        return self.Job(job_id, details=stdout, cmd=' '.join(cmd))

    def cancel(self, job_id, *args, **kwargs):
        cmd = self._process_args(['arckill'], *args, **kwargs)
        cmd.append('%s' % job_id)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)

        if not stat:
            raise jess.JobCancelError("Failed to cancel the job", ret_code=ret_code, details=stdout, cmd=' '.join(cmd))
        return self.Job(job_id, details=stdout)

    def purge(self, job_id, *args, **kwargs):
        cmd = self._process_args(['arcclean'], *args, **kwargs)
        cmd.append('%s' % job_id)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)

        if not stat:
            raise jess.JobPurgeError("Failed to purge the job", ret_code=ret_code, details=stdout, cmd=' '.join(cmd))
        return self.Job(job_id, details=stdout, cmd=' '.join(cmd))

    def status(self, job_id, *args, **kwargs):
        cmd = self._process_args(['arcstat', '-l'], *args, **kwargs)
        cmd.append('%s' % job_id)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)
        stdout = stdout.decode()
        if not stat:
            raise jess.JobStatusError("Failed to get job status", ret_code=ret_code, details=stdout, cmd=' '.join(cmd))

        match = re.search(r'Job information not found', stdout, re.M)
        if match:
            return self.Status(job_id, status="Preparing", ext_status="Preparing", details=stdout)
        match = re.search(r'This job was very recently submitted', stdout, re.M)
        if match:
            return self.Status(job_id, status="Preparing", ext_status="Preparing", details=stdout)
        match = re.search(r'\s*State\s*:\s*(\S*)', stdout, re.M)

        if not match:
            raise jess.JobStatusError('Failed to parse job status',
                                          ret_code=ret_code, details=stdout, cmd=' '.join(cmd))
        job_status = match.group(1).rstrip()
        if not job_status:
            raise jess.JobStatusError('Failed to parse job status', ret_code=ret_code, details=stdout,
                                      cmd=' '.join(cmd))
        return self.Status(job_id, status=job_status, ext_status=job_status, details=stdout)

    def logs(self, job_id, *args, **kwargs):
        # get job log - return its content
        cmd = self._process_args(['arcstat', '-l'], *args, **kwargs)
        cmd.append('%s' % job_id)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)

        if not stat:
            return self.Log(job_id, log='Failed to get job log')

        return self.Log(job_id, log=stdout)

    def output(self, job_id, *args, **kwargs):
        # retrieve output and return directory that has the output files
        # location can be specified by adding dir=<dir_prefix>
        cmd = self._process_args(['arcget', '--debug VERBOSE'], *args, **kwargs)
        cmd.append('%s' % job_id)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)
        stdout = stdout.decode()
        if not stat:
            raise jess.JobOutputError("Failed to get job output", ret_code=ret_code, details=stdout, cmd=' '.join(cmd))
        if isinstance(stdout, bytes):
            stdout = stdout 
        if 'ERROR' in stdout:
            raise jess.JobOutputError("Failed to get job output", ret_code=ret_code, details=stdout, cmd=' '.join(cmd))

        match = re.search('Results stored at: (.*)', stdout)
        if match:
            return match.group(1).rstrip()
        else:
            raise jess.JobOutputError("Failed to parse output directory", ret_code=ret_code, details=stdout,
                                      cmd=' '.join(cmd))

    @staticmethod
    def completed(status_id):
        if status_id in ["Finished"]:
            return True
        else:
            return False

    @staticmethod
    def failed(status_id):
        if status_id in ["Failed"]:
            return True
        else:
            return False

    @staticmethod
    def is_terminal(status_id):
        if status_id in ["Finished", "Failed", "Deleted", "Canceled"]:
            return True
        else:
            return False
