from __future__ import print_function
import logging
import re

import jess
import jess.backends
import jess.subproc
import jess.settings as config

log = logging.getLogger('jess')


class CREAM(jess.backends.PluginBase):
    def submit(self, jdl, *args, **kwargs):
        resource = kwargs.pop('resource', None)

        if 'resource' in jdl.keys():
            resource = jdl.pop('resource')

        if not resource:
            raise jess.JobSubmitError("Unable to determine resource from JDL or arguments")

        cmd = self._process_args(["glite-ce-job-submit"], *args, **kwargs)
        cmd.append('-r %s' % resource)
        cmd.append("%s" % jdl.jdl_path)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)
        if not stat:
            raise jess.JobSubmitError("Failed to submit the job", ret_code=ret_code, details=stdout, cmd=' '.join(cmd))

        try:
            job_id = re.search('^http.*CREAM.*$', stdout, re.M).group(0).rstrip()
        except (IndexError, AttributeError):
            # command succeeded but there is no job ID
            stdout = "{}\nJob submission has failed or timed out after {} seconds.\n".format(stdout,
                                                                                             config.SHELL_CMD_TIMEOUT)
            raise jess.JobSubmitError("Job submission failed or timed out".format(config.SHELL_CMD_TIMEOUT),
                                      ret_code=ret_code, details=stdout, cmd=' '.join(cmd))

        return self.Job(job_id, details=stdout, cmd=' '.join(cmd))

    def cancel(self, job_id, *args, **kwargs):
        cmd = self._process_args(['glite-ce-job-cancel', '--noint'], *args, **kwargs)
        cmd.append('%s' % job_id)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)

        if not stat:
            raise jess.JobCancelError("Failed to cancel the job", ret_code=ret_code, details=stdout, cmd=' '.join(cmd))
        return self.Job(job_id, details=stdout)

    def purge(self, job_id, *args, **kwargs):
        cmd = self._process_args(['glite-ce-job-purge', '--noint'], *args, **kwargs)
        cmd.append('%s' % job_id)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)

        if not stat:
            raise jess.JobPurgeError("Failed to purge the job", ret_code=ret_code, details=stdout, cmd=' '.join(cmd))
        return self.Job(job_id, details=stdout, cmd=' '.join(cmd))

    def status(self, job_id, *args, **kwargs):
        cmd = self._process_args(['glite-ce-job-status', '-L 2'], *args, **kwargs)
        cmd.append('%s' % job_id)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)

        # seems to not work correctly for glite-ce-job-status
        #if not stat:
        #    raise jess.JobStatusError("Failed to get job status", ret_code=ret_code, details=stdout, cmd=' '.join(cmd))

        match = re.search(r'\s*Status\s*=\s*\[.*\]', stdout, re.M)
        if not match:
            raise jess.JobStatusError('Failed to parse job status', ret_code=ret_code, details=stdout,
                                          cmd=' '.join(cmd))
        status_line = match.group(0).rstrip()
        st_match = re.search(r"\[([A-Z\-]+)\]", status_line)
        if not st_match:
            raise jess.JobStatusError('Failed to parse job status', ret_code=ret_code, details=stdout,
                                      cmd=' '.join(cmd))
        job_status = st_match.group(1).rstrip()
        return self.Status(job_id, status=job_status, ext_status=job_status, details=stdout)

    def logs(self, job_id, *args, **kwargs):
        # get job log - empty since cream only supports status
        return self.Log(job_id, log='')

    def output(self, job_id, *args, **kwargs):
        # retrieve output and return directory that has the output files
        # location can be specified by adding dir=<dir_prefix>
        cmd = self._process_args(['glite-ce-job-output', '--noint'], *args, **kwargs)
        cmd.append('%s' % job_id)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)

        if not stat:
            raise jess.JobOutputError("Failed to get job output", ret_code=ret_code, details=stdout, cmd=' '.join(cmd))
        if 'WARN' in stdout or 'ERROR'in stdout:
            raise jess.JobOutputError("Failed to get job output", ret_code=ret_code, details=stdout, cmd=' '.join(cmd))

        match = re.search('output will be stored in the dir (.*)$', stdout)
        if match:
            return match.group(1).rstrip()
        else:
            raise jess.JobOutputError("Failed to parse output directory", ret_code=ret_code, details=stdout,
                                      cmd=' '.join(cmd))

    @staticmethod
    def completed(status_id):
        if status_id in ["DONE-OK"]:
            return True
        else:
            return False

    @staticmethod
    def failed(status_id):
        if status_id in ["DONE-FAILED", "ABORTED"]:
            return True
        else:
            return False

    @staticmethod
    def is_terminal(status_id):
        if status_id in ["DONE-OK", "DONE-FAILED", "ABORTED", "CANCELLED"]:
            return True
        else:
            return False
