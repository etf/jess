from __future__ import print_function
import logging
import re
import os.path
import xml.etree.ElementTree as ET

import jess
import jess.backends
import jess.subproc
import jess.settings as config

log = logging.getLogger('jess')

JOB_STATE = {0: 'UNEXPANDED',
             1: 'IDLE',
             2: 'RUNNING',
             3: 'REMOVED',
             4: 'COMPLETED',
             5: 'HELD',
             6: 'TRANSFERRING OUTPUT',
             7: 'SUSPENDED'}

JOB_EVENT = {0: 'Submit',
             1: 'Execute',
             2: 'Executable error',
             3: 'Checkpointed',
             4: 'Job evicted',
             5: 'Job terminated',
             6: 'Image size updated',
             7: 'Shadow exception',
             8: 'Generic',
             9: 'Job aborted',
             10: 'Job suspended',
             11: 'Job unsuspended',
             12: 'Job held',
             13: 'Job released',
             14: 'PNode executed',
             15: 'PNode terminated',
             16: 'POST script terminated',
             17: 'Globus submit',
             18: 'Globus submit failed',
             19: 'Globus resource up',
             20: 'Globus resource down',
             21: 'Remote error',
             22: 'Remote system call socket lost',
             23: 'Remote system call socket reestablished',
             24: 'Remote system call reconnection failure',
             25: 'Grid resource up',
             26: 'Grid resource down',
             27: 'Grid Job Submitted',
             28: 'Job ad information event',
             29: 'Job remote status unknown',
             30: 'Job remote status known',
             31: 'Job stage in',
             32: 'Job stage out',
             33: 'Job ClassAdd updated',
             34: 'Pre Skip'}


class HTCondor(jess.backends.PluginBase):

    def submit(self, jdl, *args, **kwargs):

        cmd = self._process_args(['condor_submit'], *args, **kwargs)
        cmd.append('%s' % jdl.jdl_path)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)

        if not stat:
            raise jess.JobSubmitError("Failed to submit the job", ret_code=ret_code, details=stdout,
                                      cmd=' '.join(cmd))
        try:
            job_id = re.search('cluster (\d+)', stdout.decode(), re.M).groups()[0].rstrip()
        except (IndexError, AttributeError):
            # command succeeded but there is no job ID
            raise jess.JobSubmitError("Job submit result undefined, failed to parse the job ID",
                                      ret_code=ret_code, details=stdout, cmd=' '.join(cmd))

        return self.Job(job_id, details=stdout, cmd=' '.join(cmd))

    def cancel(self, job_id, *args, **kwargs):
        cmd = self._process_args(['condor_rm'], *args, **kwargs)
        cmd.append('%s' % job_id)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)

        if not stat:
            raise jess.JobCancelError("Failed to cancel the job", ret_code=ret_code, details=stdout,
                                      cmd=' '.join(cmd))
        return self.Job(job_id, details=stdout)

    def purge(self, job_id, *args, **kwargs):
        cmd = self._process_args(['condor_rm', '--forcex'], *args, **kwargs)
        cmd.append('%s' % job_id)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)

        if not stat:
            raise jess.JobPurgeError("Failed to purge the job", ret_code=ret_code, details=stdout,
                                     cmd=' '.join(cmd))
        return self.Job(job_id, details=stdout, cmd=' '.join(cmd))

    @staticmethod
    def event_status(log_file):
        with open(log_file) as logfile:
            log_xml = '<r>' + logfile.read() + '</r>'
            tree = ET.fromstring(log_xml)
            st_seq = [e.findtext('i') for e in tree.findall('c/a') if e.attrib['n'] == 'EventTypeNumber']
            return st_seq[-1]

    def status(self, job_id, *args, **kwargs):
        cmd = self._process_args(['condor_q', '-long'], *args, **kwargs)
        cmd.append('%s' % job_id)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)

        if not stat:
            raise jess.JobStatusError("Failed to get job status", ret_code=ret_code, details=stdout,
                                      cmd=' '.join(cmd))

        match = re.search(r'^\s*JobStatus\s*=\s*(\d)', stdout.decode(), re.M)
        if not match:
            raise jess.JobStatusError('Failed to parse job status', ret_code=ret_code, details=stdout,
                                          cmd=' '.join(cmd))
        job_status = match.group(1).rstrip()
        if not job_status:
            raise jess.JobStatusError('Failed to parse job status', ret_code=ret_code, details=stdout,
                                      cmd=' '.join(cmd))
        return self.Status(job_id, status=int(job_status), ext_status=JOB_STATE[int(job_status)], details=stdout)

    def logs(self, job_id, *args, **kwargs):
        if 'log' in kwargs.keys() and os.path.isfile(kwargs['log']):
            with open(kwargs['log']) as logfile:
                log_xml = '<r>' + logfile.read() + '</r>'
                tree = ET.fromstring(log_xml)
                log_seq = []
                for c in tree.findall('c'):
                    k = dict()
                    for e in c.findall('a'):
                        k[e.attrib['n']] = e.findtext('s') if e.findtext('s') else e.findtext('i')
                    log_seq.append(k)
                stdout = ""
                for e in log_seq:
                    if not job_id == e['Cluster']:
                        continue
                    log_line = '{:03d} ({}.{}) {} {}\n'.format(int(e['EventTypeNumber']), e['Cluster'],
                                                               e['Proc'], e['EventTime'], e['MyType'])
                    for k, v in e.items():
                        if k not in ['EventTypeNumber', 'Cluster', 'Proc', 'Subproc', 'EventTime', 'MyType']:
                            log_line += '     {}:{}\n'.format(k, v)
                    stdout += '\n' + log_line
            return self.Log(job_id, log=stdout)

        # classads
        if 'log' in kwargs.keys():
            kwargs.pop('log', None)   # log in jdl, but no logfile, falling back to condor_q
        cmd = self._process_args(['condor_q', '-long'], *args, **kwargs)
        cmd.append('%s' % job_id)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)
        if not stat:
            raise jess.JobStatusError("Failed to get job status/log", ret_code=ret_code, details=stdout,
                                      cmd=' '.join(cmd))

        return self.Log(job_id, log=stdout)

    def output(self, job_id, *args, **kwargs):
        # output directory is part of jdl and managed by condor, so this is usually not needed
        pass

    def transfer_data(self, job_id, *args, **kwargs):
        # explicit transfer for direct condor-ce submission
        cmd = self._process_legacy_args(['condor_transfer_data', ], *args, **kwargs)
        cmd.append('%s' % job_id)

        stat, ret_code, stdout = jess.subproc.run(cmd, cmd_timeout=config.SHELL_CMD_TIMEOUT)
        if not stat:
            raise jess.JobStatusError("Failed to transfer data", ret_code=ret_code, details=stdout,
                                      cmd=' '.join(cmd))

        return self.Job(job_id, details=stdout, cmd=' '.join(cmd))

    @staticmethod
    def completed(status_id):
        if status_id in [4, 6]:
            return True
        else:
            return False

    @staticmethod
    def failed(status_id):
        if status_id == 5:
            return True
        else:
            return False

    @staticmethod
    def is_terminal(status_id):
        if status_id in [3, 4, 5, 6]:
            return True
        else:
            return False
