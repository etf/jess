import unittest
import logging
import sys

import jess.api

log = logging.getLogger("jess")
log.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(message)s')
if sys.version_info[1] <= 6:
    fh = logging.StreamHandler(strm=sys.stdout)
else:
    fh = logging.StreamHandler(stream=sys.stdout)
fh.setFormatter(formatter)
log.addHandler(fh)


class APITests(unittest.TestCase):

    def test_htcondor(self):
        job = jess.api.scondor.submit({})
        status = jess.api.scondor.status(job.job_id)


