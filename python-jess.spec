Summary: python grid job submission library/api
Name: python3-jess
Version: 0.2.58
Release: 1%{?dist}
Source: %{name}-%{version}.tar.gz
License: ASL 2.0
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildArch: noarch
Requires: python3-pexpect
Requires: python3-nap
Url: https://gitlab.cern.ch/etf/jess.git
BuildRequires: python3-setuptools python3-devel /usr/bin/pathfix.py
%description

Python grid job submission API supporting ARC, HT-Condor, CREAM, etc.


%prep
%autosetup -n %{name}-%{version}
pathfix.py -pni "%{__python3} %{py3_shbang_opts}" . bin/check_js bin/check_ce_ping bin/check_js_reset

%build
%py3_build

%install
%{__python3} setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES

%clean
rm -rf $RPM_BUILD_ROOT

%files -f INSTALLED_FILES
%defattr(-,root,root)
%doc README.md
