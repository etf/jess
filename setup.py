import jess
import os

from setuptools import setup

NAME = 'python3-jess'
VERSION = jess.VERSION
DESCRIPTION = "Python Grid Job Submission Library"
LONG_DESCRIPTION = """
python grid job submission API supporting ARC, CREAM, HTCONDOR, etc.
"""
AUTHOR = jess.AUTHOR
AUTHOR_EMAIL = jess.AUTHOR_EMAIL
LICENSE = "ASL 2.0"
PLATFORMS = "Any"
URL = "https://gitlab.cern.ch/etf/jess.git"
CLASSIFIERS = [
    "Development Status :: 5 - Production/Stable",
    "License :: OSI Approved :: Apache Software License",
    "Operating System :: Unix",
    "Programming Language :: Python",
    "Programming Language :: Python :: 2.6",
    "Programming Language :: Python :: 2.7",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.0",
    "Programming Language :: Python :: 3.1",
    "Programming Language :: Python :: 3.2",
    "Programming Language :: Python :: 3.3",
    "Topic :: Software Development :: Libraries :: Python Modules"
]


def get_files(install_prefix, directory):
    files = []
    for root, dirnames, filenames in os.walk(directory):
        subdir_files = []
        for filename in filenames:
            subdir_files.append(os.path.join(root, filename))
        if filenames and subdir_files:
            rel_path = os.path.relpath(root, directory)
            if rel_path != '.':
                files.append((os.path.join(install_prefix, rel_path), subdir_files))
            else:
                files.append((install_prefix, subdir_files))
    return files


setup(name=NAME,
      version=VERSION,
      description=DESCRIPTION,
      long_description=LONG_DESCRIPTION,
      author=AUTHOR,
      author_email=AUTHOR_EMAIL,
      license=LICENSE,
      platforms=PLATFORMS,
      url=URL,
      classifiers=CLASSIFIERS,
      keywords='job grid condor cream arc docker',
      packages=['jess', 'jess.backends'],
      install_requires=['future', 'pexpect'],
      data_files=[
            ('/usr/lib64/nagios/plugins', ['bin/check_js', 'bin/check_js_reset', 'bin/check_ce_ping'])])
